const operadores = require("../src/index");

describe('Testa o operandor soma', function(){
  it('Testa que 5 + 6 deve ser 11', function(){
     expect(operadores.soma(5,6)).toBe(11);
     expect(operadores.soma(2,3)).toBeGreaterThan(4);
  });
});